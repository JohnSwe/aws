import boto3
import argparse


def start_stop_instances(instance, state):
    if args.state == 'start':
        instance.start()
        print('Starting instance: ', name, 'ID: ', instance.id)
    if args.state == 'stop':
        instance.stop()
        print('Stopping instance: ', name, 'ID: ', instance.id)


parser = argparse.ArgumentParser()
parser.add_argument('--state', '-s', choices=['start', 'stop'], type=str, required=True, help='Options: [start|stop]')
parser.add_argument('--tag', '-t', type=str, help='Specify which group of machines to start or stop')
args = parser.parse_args()

ec2_client = boto3.client('ec2')

region_dict = ec2_client.describe_regions()
region_all_info = region_dict['Regions']
region_list = []

for region in region_all_info:
    region_list.append(region['RegionName'])

for region in region_list:
    ec2 = boto3.resource('ec2', region_name=region)

    print('Region: ', region)

    instances = ec2.instances.all()  #.filter( Filters=[{'Name': 'instance-state-name', 'Values': [args.state]} ])

    for instance in instances:
        name = ""
        for tag in instance.tags:
            if tag['Key']  == 'Name':
                name = tag['Value']

        if args.tag:
            if args.tag in name:
                start_stop_instances(instance, args.state)
        else:
            start_stop_instances(instance, args.state)